{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Networking.Statuscodes where

import GHC.Generics
import Data.Aeson

data Statuscode
    = Ok
    | SignupSuccessful
    | AuthorizationSuccessful
    | AuthorizationRequired
    | UsernameAlreadyTaken
    | InvalidCredentials
    | RecipientDoesNotExist
    | GroupNameAlreadyTaken
    | GroupDoesNotExist
    | UserIsNotInGroup
    | MalformedRequest
    | InternalServerError
    deriving (Show, Eq, Enum, Generic, ToJSON, FromJSON)
