{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Networking.Response
( Response (..)
, ResponseBody(..)
, encodeResponse
, decodeResponse
, okResponse
, emptyResponse
, Message (..)
) where

import Data.Word
import Data.Bits
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T
import GHC.Generics
import Data.Aeson
import Data.Time
import Networking.Statuscodes

data Response = Response
                    { version :: Float
                    , status :: Statuscode
                    , body :: ResponseBody
                    } deriving (Eq, Show, Generic, ToJSON, FromJSON)

data ResponseBody = EmptyResponseBody
                  | DMResponseBody { directMessages :: [Message] }
                  | GroupMessagesResponseBody
                    { group :: T.Text
                    , groupMessages :: [Message]
                    }
                  deriving (Eq, Show, Generic, ToJSON, FromJSON)

data Message = Message
    { sender :: T.Text
    , message :: T.Text
    , timestamp :: UTCTime
    } deriving (Eq, Show, Generic, ToJSON, FromJSON)

encodeResponse :: Response -> BS.ByteString
encodeResponse = BSL.toStrict . encode

okResponse :: Response
okResponse = Response 0.3 Networking.Statuscodes.Ok EmptyResponseBody

emptyResponse :: Statuscode -> Response
emptyResponse status = Response 0.3 status EmptyResponseBody

decodeResponse :: BS.ByteString -> Maybe Response
decodeResponse = decode . BSL.fromStrict
