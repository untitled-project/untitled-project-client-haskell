{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
module Networking.Request where

import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Data.Time
import GHC.Generics
import Data.Aeson

data Request = Request
        { version :: Float
        , body :: RequestBody
        } deriving (Eq, Show, Generic, ToJSON, FromJSON)

data RequestBody = AuthRequest
                    { username :: T.Text
                    , password :: T.Text
                    }
                 | SendDMRequest
                    { receiver :: T.Text
                    , message :: T.Text
                    }
                 | SignUpRequest
                    { username :: T.Text
                    , password :: T.Text
                    }
                 | GetDMRequest
                    { since :: UTCTime
                    }
                 | GetAllDMRequest
                 | CreateGroupRequest
                    { groupName :: T.Text
                    }
                 | JoinGroupRequest
                    { groupName :: T.Text
                    }
                 | GetGroupMessagesRequest
                    { groupName :: T.Text
                    , since :: UTCTime
                    }
                 | GetAllGroupMessagesRequest
                    { groupName :: T.Text
                    }
                 deriving (Eq, Show, Generic, ToJSON, FromJSON)

readRequest :: BS.ByteString -> Maybe Request
readRequest = decode . BSL.fromStrict

encodeRequest :: Request -> BS.ByteString
encodeRequest = BSL.toStrict . encode
