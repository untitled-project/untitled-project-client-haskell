{-# LANGUAGE OverloadedStrings #-}
module Util.Authentication where

import qualified Data.Text as T
import Networking.Request
import Network.Socket hiding (recv)
import Network.Socket.ByteString
import Networking.Response
import Networking.Statuscodes

authenticate :: Socket -> T.Text -> T.Text -> IO Bool
authenticate sock username password = do
    sendAll sock $ encodeRequest $ Request 0.3 (AuthRequest username password)
    response <- decodeResponse <$> recv sock 1024
    case response of 
        Just (Response 0.3 Ok _) -> return True
        _                        -> return False
            
