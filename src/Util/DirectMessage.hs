module Util.DirectMessage where

import qualified Data.Text as T
import Network.Socket hiding (recv)
import Network.Socket.ByteString
import Networking.Request
import Networking.Response
import Networking.Statuscodes

sendDirectMessage :: Socket -> T.Text -> T.Text -> IO Bool
sendDirectMessage sock recipient msg = do
    sendAll sock $ encodeRequest $ Request 0.3 (SendDMRequest recipient msg)
    response <- decodeResponse <$> recv sock 1024
    case response of
        Just (Response _ Ok _) -> return True
        _                       -> return False

fetchAllDirectMessages :: Socket -> IO [Message]
fetchAllDirectMessages sock = do
    sendAll sock $ encodeRequest $ Request 0.3 GetAllDMRequest
    mResponse <- decodeResponse <$> recv sock 1024
    case mResponse of
        Just (Response _ Ok response) -> return $ directMessages response
        _ -> return []
