module Util.Registration where

import qualified Data.Text as T
import Network.Socket hiding (recv)
import Network.Socket.ByteString 
import Networking.Request
import Networking.Response
import Networking.Statuscodes

register :: Socket -> T.Text -> T.Text -> IO Bool
register sock username password = do
    sendAll sock $ encodeRequest $ Request 0.3 (SignUpRequest username password) 
    answer <- recv sock 1024
    print answer
    case decodeResponse answer of
        Just (Response _ Ok _) -> return True
        _                      -> return False

