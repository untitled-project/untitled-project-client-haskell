{-# LANGUAGE LambdaCase #-}
module UI where

import Brick
import Brick.Widgets.Center
import Brick.Widgets.Border
import Brick.Widgets.Border.Style
import Brick.Forms
import qualified Graphics.Vty as V
import qualified Data.Text as T
import Brick.Focus
import qualified Brick.Widgets.Edit as E
import UI.Types
import UI.LoginMenu
import UI.MainMenu

app :: App State () Name
app = App
    { appDraw = UI.render
    , appChooseCursor = \state -> 
        case state of
            MainMenu -> neverShowCursor MainMenu
            (LoginMenu form) -> focusRingCursor formFocus form
            RegisterMenu -> neverShowCursor RegisterMenu
            _ -> neverShowCursor state
    , appHandleEvent = handleEvent
    , appStartEvent = return 
    , appAttrMap = const attributeMap
    }

render :: State -> [Widget Name]
render MainMenu = renderMainMenu MainMenu
render state@(LoginMenu form) = renderLoginMenu state
render state@(ConnectionFailed) = renderString "Connection failed."
render state = renderString "Not yet implemented."

handleEvent :: State -> BrickEvent Name () -> EventM Name (Next State)
handleEvent state@(MainMenu) e = handleMainMenu state e
handleEvent state@(LoginMenu _) e  = handleLoginMenu state e
handleEvent state@(RegisterMenu) e = handleRegisterMenu state e
handleEvent state (VtyEvent (V.EvKey _ _ )) = halt state
handleEvent state _ = continue state

handleRegisterMenu :: State -> BrickEvent Name () -> EventM Name (Next State)
handleRegisterMenu state (VtyEvent (V.EvKey V.KEsc [])) = halt state 
handleRegisterMenu state _ = continue state

renderString :: String -> [Widget n]
renderString s =
    [ withBorderStyle unicode
    $ border
    $ center (str s)
    ]
