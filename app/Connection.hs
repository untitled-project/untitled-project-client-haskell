module Connection where

import Network.Socket

connect :: IO Socket
connect = do
    addr <- resolve "127.0.0.1" "2599"
    open addr
        where
            resolve host port = do
                let hints = defaultHints { addrSocketType = Stream }
                addr:_ <- getAddrInfo (Just hints) (Just host) (Just port)
                return addr
            open addr = do
                sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
                Network.Socket.connect sock $ addrAddress addr
                return sock
