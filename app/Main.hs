{-# LANGUAGE OverloadedStrings #-}
module Main where

import Text.Read
import qualified Data.Text.IO as T.IO
import Util.Authentication
import Util.Registration
import Util.DirectMessage
import Network.Socket
import qualified Network.Socket.ByteString as SBS
import qualified Control.Exception as E (bracket)
import Brick
import UI
import Control.Monad (void)
import Graphics.Vty as V
import UI.Types

main :: IO ()
main = do
    initialVty <- buildVty
    void $ customMain initialVty buildVty Nothing app MainMenu
    where
        buildVty = do
            v <- V.mkVty =<< V.standardIOConfig
            V.setMode (V.outputIface v) V.Mouse True
            return v

