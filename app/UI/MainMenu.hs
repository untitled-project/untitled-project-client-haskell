module UI.MainMenu
    ( handleMainMenu
    , renderMainMenu
    ) where

import UI.Types
import Brick
import qualified Graphics.Vty as V
import Brick.Widgets.Border.Style
import Brick.Widgets.Border
import Brick.Widgets.Center

handleMainMenu :: State -> BrickEvent Name () -> EventM Name (Next State)
handleMainMenu state (VtyEvent (V.EvKey (V.KChar 'l') [])) = continue (LoginMenu initialLoginForm)
handleMainMenu state (VtyEvent (V.EvKey (V.KChar 'r') [])) = continue (RegisterMenu)
handleMainMenu state (MouseUp LoginMenuButton button coords) = continue (LoginMenu initialLoginForm)
handleMainMenu state (MouseUp RegisterMenuButton button coords) = continue RegisterMenu
handleMainMenu state (VtyEvent (V.EvKey V.KEsc [])) = halt state 
handleMainMenu state _ = continue state

renderMainMenu :: State -> [Widget Name]
renderMainMenu MainMenu =
    [ withBorderStyle unicode
    $ border
    $ center
    $ loginMenuButton <+> padLeft (Pad 5) registerMenuButton
    ]
renderMainMenu _ = undefined

loginMenuButton :: Widget Name
loginMenuButton
    = clickable LoginMenuButton
    $ border
    $ padLeftRight 4 
    $ padTopBottom 1
    $ str "Login"

registerMenuButton :: Widget Name
registerMenuButton
    = clickable RegisterMenuButton
    $ border
    $ padLeftRight 2 
    $ padTopBottom 1
    $ str " Register "
