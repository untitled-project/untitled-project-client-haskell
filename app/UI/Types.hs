{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
module UI.Types where

import Brick
import Brick.Forms
import Brick.Widgets.Border
import qualified Data.Text as T
import Lens.Micro.TH
import qualified Graphics.Vty as V
import qualified Brick.Widgets.Edit as E
import Brick.AttrMap
import Network.Socket

data State
    = MainMenu
    | LoginMenu (Form Credentials () Name)
    | RegisterMenu
    | ActionMenu Socket
    | ConnectionFailed
    | InvalidCredentials

data Name
    = Username
    | Password
    | LoginButton
    | LoginMenuButton
    | RegisterMenuButton
    deriving (Show, Eq, Ord)

data Credentials = Credentials
    { _username :: T.Text
    , _password :: T.Text
    } deriving (Show)

makeLenses ''Credentials

mkLoginForm :: Credentials -> Form Credentials () Name
mkLoginForm = newForm
    [ (label "Username: ") @@= editTextField username Username Nothing
    , (label "Password: ") @@= editPasswordField password Password
    ]
        where
            label s w = padBottom (Pad 1) $ (padTop (Pad 1) $ vLimit 1 $ hLimit 15 $ str s <+> fill ' ') <+> (border $ vLimit 1 $ hLimit 50 $ w)

initialLoginForm :: Form Credentials () Name
initialLoginForm = mkLoginForm (Credentials "" "")

attributeMap :: AttrMap
attributeMap = attrMap V.defAttr
    [ (E.editAttr, V.white `on` V.black)
    , (E.editFocusedAttr, V.white `on` V.brightBlack)
    , (invalidFormInputAttr, V.white `on` V.red)
    , (focusedFormInputAttr, V.black `on` V.yellow)
    , (attrName "button", V.white `on`V.brightBlack)
    ]


