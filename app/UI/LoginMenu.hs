module UI.LoginMenu
    ( handleLoginMenu
    , Credentials (..)
    , renderLoginMenu
    ) where

import UI.Types
import qualified Graphics.Vty as V
import Brick
import Brick.Forms
import Brick.Widgets.Center
import Brick.Widgets.Border
import Brick.Widgets.Border.Style
import Lens.Micro ((^.))
import Connection
import Control.Exception (try, IOException)
import Util.Authentication
import Network.Socket

handleLoginMenu :: State -> BrickEvent Name () -> EventM Name (Next State)
handleLoginMenu state (VtyEvent (V.EvKey V.KEsc [])) = halt state 
handleLoginMenu state@(LoginMenu form) (VtyEvent (V.EvKey V.KEnter [])) = suspendAndResume (checkCreds $ formState form)
handleLoginMenu state@(LoginMenu form) (MouseUp LoginButton button coords) = suspendAndResume (checkCreds $ formState form)
handleLoginMenu state@(LoginMenu form) event = handleFormEvent event form >>= \form -> continue (LoginMenu form)
handleLoginMenu state _ = continue state

renderLoginMenu :: State -> [Widget Name]
renderLoginMenu (LoginMenu form) = [ withBorderStyle unicode $ border
    $ vCenter $ hCenter (renderForm form) <=> hCenter loginButton 
    ]
renderLoginMenu _ = undefined

loginButton :: Widget Name
loginButton
    = clickable LoginButton
    $ border
    $ padLeftRight 1
    $ str "Login"

checkCreds :: Credentials -> IO State
checkCreds creds = do
    conn <- (try Connection.connect :: IO (Either IOException Socket))
    case conn of
        Left _ -> return ConnectionFailed
        Right sock -> do
            success <- authenticate sock (creds^.username) (creds^.password)
            if success then
                return $ ActionMenu sock
            else
                return InvalidCredentials
